<?php

namespace LibreByte\Tests;

use LibreByte\XML\Reader;
use LibreByte\XML\Node;

class SimpleTest
{
    public static function create()
    {
        return new static();
    }

    public function getFirtsNode()
    {
        $xml = <<<'XML'
            <?xml version="1.0" encoding="UTF-8"?>
            <records>
                <record>
                    <Name>Nicole</Name>
                    <Company>Ac Mattis LLC</Company>
                    <Address>Ap #823-8881 Adipiscing Avenue</Address>
                    <City>Fontanellato</City>
                    <Country>Puerto Rico</Country>
                    <Phone>(444) 834-6922</Phone>
                    <Geo>5.84145, -13.30146</Geo>
                    <Description>auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu.</Description>
                </record>
                <record>
                    <Name>Brady</Name>
                    <Company>Nullam Enim Institute</Company>
                    <Address>818-4904 Lectus Av.</Address>
                    <City>Jennersdorf</City>
                    <Country>Virgin Islands, British</Country>
                    <Phone>(583) 930-1188</Phone>
                    <Geo>-24.42546, 145.2476</Geo>
                    <Description>et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque.</Description>
                </record>
            </records>
        XML;

        $reader =  Reader::fromString($xml);

        assert($reader->read('records/record')->current() instanceof Node);

        success(__FUNCTION__);
    }

    public function readSimpleXMLChildValue()
    {
        $xml = <<<'XML'
            <?xml version="1.0" encoding="UTF-8"?>
            <records>
                <record>
                    <Name>Nicole</Name>
                    <Company>Ac Mattis LLC</Company>
                    <Address>Ap #823-8881 Adipiscing Avenue</Address>
                    <City>Fontanellato</City>
                    <Country>Puerto Rico</Country>
                    <Phone>(444) 834-6922</Phone>
                    <Geo>5.84145, -13.30146</Geo>
                    <Description>auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu.</Description>
                </record>
                <record>
                    <Name>Brady</Name>
                    <Company>Nullam Enim Institute</Company>
                    <Address>818-4904 Lectus Av.</Address>
                    <City>Jennersdorf</City>
                    <Country>Virgin Islands, British</Country>
                    <Phone>(583) 930-1188</Phone>
                    <Geo>-24.42546, 145.2476</Geo>
                    <Description>et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque.</Description>
                </record>
            </records>
        XML;

        $reader =  Reader::fromString($xml);

        foreach ($reader->read('records/record') as $node) {
             assert(in_array($node->getChildValue('Name'), ['Nicole', 'Brady']));
        }

        success(__FUNCTION__);
    }


    public function readSimpleXMLAttribute()
    {
        // XML example from https://gist.github.com/jasonbaldridge/2597611
        $xml = <<<'XML'
            <music>
              <artist name="Radiohead">
                <album title="The King of Limbs">
                  <song title="Bloom" length="5:15" />
                  <song title="Morning Mr Magpie" length="4:41" />
                </album>
                <album title="OK Computer">
                  <song title="Airbag" length="4:44" />
                  <song title="Paranoid Android" length="6:23" />
                </album>
              </artist>
            </music>
        XML;

        $reader =  Reader::fromString($xml);

        $result = true;
        foreach ($reader->read('music/artist') as $artistNode) {
            assert($artistNode->getName() == 'artist');
            assert($artistNode->getAttribute('name') == 'Radiohead');
            foreach ($artistNode->getChildren() as $albumNode) {
                assert($albumNode->getName() == 'album');
                assert(in_array($albumNode->getAttribute('title'), ['The King of Limbs', 'OK Computer']));
                assert(in_array($albumNode->getChildValue('song/@title'), ['Bloom', 'Airbag']));
            }

            foreach ($artistNode->getChildren('album/song') as $songNode) {
                assert(in_array($songNode->getAttribute('title'), ['Bloom', 'Morning Mr Magpie', 'Airbag', 'Paranoid Android']));
            }
        }
        success(__FUNCTION__);
    }

    public function readXMLWithNamespace()
    {
        // XML example from http://www.datypic.com/books/defxmlschema/chapter03.html (Example 3-13.)
        $xml = <<<'XML'
        <envelope>
          <order xmlns="http://datypic.com/ord"
                 xmlns:prod="http://datypic.com/prod">
            <number>123ABBCC123</number>
            <items>
              <product xmlns="http://datypic.com/prod">
                <number prod:id="prod557">557</number>
                <name xmlns="">Short-Sleeved Linen Blouse</name>
                <prod:size system="US-DRESS">10</prod:size>
                <prod:color xmlns:prod="http://datypic.com/prod2"
                            prod:value="blue"/>
              </product>
            </items>
          </order>
        </envelope>
        XML;

        $reader = Reader::fromString($xml);
        foreach ($reader->read('envelope/order') as $orderNode) {
            assert($orderNode->getName() == 'order');
            // Register a prefix por default namespace
            $orderNode->registerNamespace('ns', 'http://datypic.com/ord');
            assert($orderNode->getChild('ns:number')->getValue() == '123ABBCC123');
            assert($orderNode->getChildValue('ns:number') == '123ABBCC123');

            $itemNode = $orderNode->getChild('ns:items');
            // Register a prefix por default namespace
            $itemNode->registerNamespace('ns', 'http://datypic.com/prod');
            $prodNode = $itemNode->getChild('ns:product');

            // Register a prefix por default namespace
            $prodNode->registerNamespace('ns', 'http://datypic.com/prod');
            assert($prodNode->getChildValue('ns:number') == 557);
            assert($prodNode->getChildValue('ns:number/@prod:id') == 'prod557');
            assert($prodNode->getChildValue('name') == 'Short-Sleeved Linen Blouse');
            assert($prodNode->getChild('prod:size/@system'), 'US-DRESS');
            $prodNode->registerNamespace('prod2', 'http://datypic.com/prod2');
            assert($prodNode->getChildValue('prod2:color/@prod2:value') == 'blue');

            foreach ($prodNode->getChildren() as $chdNode) {
                assert(in_array($chdNode->getName(), ['number', 'name']));
            }

            foreach ($prodNode->getChildren('prod', [], true) as $chdNode) {
                assert(in_array($chdNode->getName(), ['size', 'color']));
            }
        }

        success(__FUNCTION__);
    }

    // https://dumps.wikimedia.org/wikidatawiki/20210501/
    public function readBigXMLFile()
    {
        /*
         * XML_BIG_FILE_PATH must be defined as enviroment variable
         * ON GNU/Linux and other UNIX OS Type do an
         * export XML_BIG_FILE_PATH="$(pwd)/wikidatawiki-pages.xml"
         * from your terminal
         */
        if (!empty($filePath = getenv('XML_BIG_FILE_PATH', true))) {
            $reader = Reader::fromFile($filePath);
            $i = 0;
            foreach ($reader->read('mediawiki/page') as $pageNode) {
                $pageNode->registerNamespace('ns', 'http://www.mediawiki.org/xml/export-0.10/');
                if (CLI) {
                    progress_bar(++$i, 248787, '(Reading big file)');
                }
            }
            echo NL;
            assert($i == 248787);
            success(__FUNCTION__);
        }
    }

    public static function autorun()
    {
        $instance = static::create();

        $instance->getFirtsNode();
        $instance->readSimpleXMLChildValue();
        $instance->readSimpleXMLAttribute();
        $instance->readXMLWithNamespace();
        $instance->readBigXMLFile();
    }
}
